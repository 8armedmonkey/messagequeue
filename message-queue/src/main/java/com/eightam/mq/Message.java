package com.eightam.mq;

public interface Message {

    String getId();

    TimeToLive getTimeToLive();

}
