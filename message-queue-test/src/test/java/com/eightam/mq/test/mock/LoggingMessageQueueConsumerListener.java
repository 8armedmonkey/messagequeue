package com.eightam.mq.test.mock;

import com.eightam.mq.Message;
import com.eightam.mq.MessageQueueConsumer;

public class LoggingMessageQueueConsumerListener implements MessageQueueConsumer.Listener {

    private boolean onStartCalled;
    private boolean onConsumeCalled;
    private boolean onStopCalled;
    private boolean onNothingToConsumeCalled;

    @Override
    public void onStart() {
        onStartCalled = true;
        System.out.println(">>>>> [" + System.currentTimeMillis() + "] onStart");
    }

    @Override
    public void onConsume(Message message) {
        onConsumeCalled = true;
        System.out.println(">>>>> [" + System.currentTimeMillis() + "] onConsume, m: " + message.getId());
    }

    @Override
    public void onNothingToConsume() {
        onNothingToConsumeCalled = true;
        System.out.println(">>>>> [" + System.currentTimeMillis() + "] onNothingToConsume");
    }

    @Override
    public void onStop() {
        onStopCalled = true;
        System.out.println(">>>>> [" + System.currentTimeMillis() + "] onStop");
    }

    public boolean isOnStartCalled() {
        return onStartCalled;
    }

    public void setOnStartCalled(boolean onStartCalled) {
        this.onStartCalled = onStartCalled;
    }

    public boolean isOnConsumeCalled() {
        return onConsumeCalled;
    }

    public void setOnConsumeCalled(boolean onConsumeCalled) {
        this.onConsumeCalled = onConsumeCalled;
    }

    public boolean isOnStopCalled() {
        return onStopCalled;
    }

    public void setOnStopCalled(boolean onStopCalled) {
        this.onStopCalled = onStopCalled;
    }

    public boolean isOnNothingToConsumeCalled() {
        return onNothingToConsumeCalled;
    }

    public void setOnNothingToConsumeCalled(boolean onNothingToConsumeCalled) {
        this.onNothingToConsumeCalled = onNothingToConsumeCalled;
    }

}
