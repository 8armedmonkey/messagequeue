package com.eightam.mq;

public class TimeToLiveIndefinite implements TimeToLive {

    @Override
    public Type getType() {
        return Type.INDEFINITE;
    }

}
