package com.eightam.mq;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.ConcurrentLinkedQueue;

public class MessageQueue {

    private Queue<Message> queue;
    private Map<String, Message> lookup;
    private Set<Listener> listeners;

    public MessageQueue() {
        queue = new ConcurrentLinkedQueue<>();
        lookup = new HashMap<>();
        listeners = new HashSet<>();
    }

    public void offer(Message m) {
        queue.offer(m);

        if (m.getId() != null && !m.getId().isEmpty()) {
            lookup.put(m.getId(), m);
        }

        notifyListenersOnMessageOffered();
    }

    public Message poll() {
        Message m = queue.poll();

        if (m != null) {
            lookup.remove(m.getId());
        }
        return m;
    }

    public Message peek() {
        return queue.peek();
    }

    public boolean contains(String messageId) {
        return lookup.get(messageId) != null;
    }

    public void remove(String messageId) {
        Message m = lookup.get(messageId);

        if (m != null) {
            queue.remove(m);
            lookup.remove(messageId);
        }
    }

    public void clear() {
        queue.clear();
        lookup.clear();
    }

    public int size() {
        return queue.size();
    }

    public void registerListener(Listener listener) {
        listeners.add(listener);
    }

    public void unregisterListener(Listener listener) {
        listeners.remove(listener);
    }

    private void notifyListenersOnMessageOffered() {
        for (Listener listener : listeners) {
            listener.onMessageOffered();
        }
    }

    public interface Listener {

        void onMessageOffered();

    }

}
