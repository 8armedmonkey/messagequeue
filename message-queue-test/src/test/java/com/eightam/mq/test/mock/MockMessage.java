package com.eightam.mq.test.mock;

import com.eightam.mq.Message;
import com.eightam.mq.TimeToLive;

public class MockMessage implements Message {

    private String id;
    private TimeToLive timeToLive;

    public MockMessage(String id, TimeToLive timeToLive) {
        this.id = id;
        this.timeToLive = timeToLive;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public TimeToLive getTimeToLive() {
        return timeToLive;
    }

}
