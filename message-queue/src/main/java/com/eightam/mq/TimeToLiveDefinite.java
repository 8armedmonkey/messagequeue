package com.eightam.mq;

public class TimeToLiveDefinite implements TimeToLive {

    private long durationMillis;

    public TimeToLiveDefinite(long durationMillis) {
        this.durationMillis = durationMillis;
    }

    @Override
    public Type getType() {
        return Type.DEFINITE;
    }

    public long getDurationMillis() {
        return durationMillis;
    }

}
