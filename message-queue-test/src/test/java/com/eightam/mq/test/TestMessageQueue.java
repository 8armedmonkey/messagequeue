package com.eightam.mq.test;

import com.eightam.mq.MessageQueue;
import com.eightam.mq.MessageQueueConsumer;
import com.eightam.mq.TimeToLiveDefinite;
import com.eightam.mq.TimeToLiveIndefinite;
import com.eightam.mq.test.mock.LoggingMessageQueueConsumerListener;
import com.eightam.mq.test.mock.MockMessage;

import org.junit.Assert;
import org.junit.Test;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

public class TestMessageQueue {

    @Test
    public void testConsumeMessage() throws InterruptedException {
        System.out.println("\n***** TestMessageQueue.testConsumeMessage\n");

        CountDownLatch countDownLatch = new CountDownLatch(1);

        MessageQueue messageQueue = new MessageQueue();

        MessageQueueConsumer messageQueueConsumer = new MessageQueueConsumer(messageQueue);
        messageQueueConsumer.setListener(new LoggingMessageQueueConsumerListener());

        messageQueue.offer(new MockMessage("MSG-1", new TimeToLiveDefinite(2000)));
        messageQueue.offer(new MockMessage("MSG-2", new TimeToLiveDefinite(3000)));
        messageQueue.offer(new MockMessage("MSG-3", new TimeToLiveIndefinite()));
        messageQueue.offer(new MockMessage("MSG-4", new TimeToLiveDefinite(4000)));
        messageQueue.offer(new MockMessage("MSG-5", new TimeToLiveDefinite(2000)));
        messageQueue.offer(new MockMessage("MSG-6", new TimeToLiveIndefinite()));
        messageQueue.offer(new MockMessage("MSG-7", new TimeToLiveDefinite(1000)));

        messageQueueConsumer.start();

        countDownLatch.await(8000, TimeUnit.MILLISECONDS);

        Assert.assertTrue(messageQueue.peek().getId().equals("MSG-4"));
        messageQueueConsumer.consume();

        countDownLatch.await(8000, TimeUnit.MILLISECONDS);

        Assert.assertTrue(messageQueue.peek().getId().equals("MSG-7"));
        messageQueueConsumer.consume();

        countDownLatch.await(2000, TimeUnit.MILLISECONDS);
        messageQueueConsumer.stop();

        Assert.assertTrue(messageQueue.size() == 0);

        System.out.println("\n--------------------------------------------------");
    }

    @Test
    public void testStopMessageQueueConsumer() throws InterruptedException {
        System.out.println("\n***** TestMessageQueue.testStopMessageQueueConsumer\n");

        CountDownLatch countDownLatch = new CountDownLatch(1);

        MessageQueue messageQueue = new MessageQueue();

        MessageQueueConsumer messageQueueConsumer = new MessageQueueConsumer(messageQueue);
        messageQueueConsumer.setListener(new LoggingMessageQueueConsumerListener());

        messageQueue.offer(new MockMessage("MSG-1", new TimeToLiveDefinite(2000)));
        messageQueue.offer(new MockMessage("MSG-2", new TimeToLiveDefinite(3000)));
        messageQueue.offer(new MockMessage("MSG-3", new TimeToLiveDefinite(2000)));

        messageQueueConsumer.start();

        countDownLatch.await(4000, TimeUnit.MILLISECONDS);
        messageQueueConsumer.stop();

        Assert.assertTrue(messageQueue.peek().getId().equals("MSG-3"));

        System.out.println("\n--------------------------------------------------");
    }

    @Test
    public void testMessageOfferedNotificationConsumedDirectly() {
        System.out.println("\n***** TestMessageQueue.testMessageOfferedNotificationConsumedDirectly\n");

        MessageQueue messageQueue = new MessageQueue();

        MessageQueueConsumer messageQueueConsumer = new MessageQueueConsumer(messageQueue);
        messageQueueConsumer.setListener(new LoggingMessageQueueConsumerListener());

        messageQueueConsumer.start();

        messageQueue.offer(new MockMessage("MSG-1", new TimeToLiveDefinite(2000)));
        messageQueue.offer(new MockMessage("MSG-2", new TimeToLiveDefinite(3000)));

        messageQueueConsumer.stop();
        Assert.assertTrue(messageQueue.peek().getId().equals("MSG-2"));

        System.out.println("\n--------------------------------------------------");
    }

    @Test
    public void testMessageOfferedNotificationConsumedLater() {
        System.out.println("\n***** TestMessageQueue.testMessageOfferedNotificationConsumedLater\n");

        MessageQueue messageQueue = new MessageQueue();

        MessageQueueConsumer messageQueueConsumer = new MessageQueueConsumer(messageQueue);
        messageQueueConsumer.setListener(new LoggingMessageQueueConsumerListener());

        messageQueue.offer(new MockMessage("MSG-1", new TimeToLiveDefinite(2000)));
        messageQueue.offer(new MockMessage("MSG-2", new TimeToLiveDefinite(3000)));

        messageQueueConsumer.start();

        messageQueue.offer(new MockMessage("MSG-3", new TimeToLiveDefinite(2000)));

        messageQueueConsumer.stop();
        Assert.assertTrue(messageQueue.peek().getId().equals("MSG-2"));

        System.out.println("\n--------------------------------------------------");
    }

    @Test
    public void testMessageRemoved() throws InterruptedException {
        System.out.println("\n***** TestMessageQueue.testMessageRemoved\n");

        CountDownLatch countDownLatch = new CountDownLatch(1);

        MessageQueue messageQueue = new MessageQueue();

        MessageQueueConsumer messageQueueConsumer = new MessageQueueConsumer(messageQueue);
        messageQueueConsumer.setListener(new LoggingMessageQueueConsumerListener());

        messageQueue.offer(new MockMessage("MSG-1", new TimeToLiveDefinite(2000)));
        messageQueue.offer(new MockMessage("MSG-2", new TimeToLiveDefinite(4000)));
        messageQueue.offer(new MockMessage("MSG-3", new TimeToLiveDefinite(2000)));
        messageQueue.offer(new MockMessage("MSG-4", new TimeToLiveDefinite(3000)));

        messageQueueConsumer.start();

        messageQueue.remove("MSG-3");
        countDownLatch.await(5000, TimeUnit.MILLISECONDS);

        messageQueueConsumer.stop();
        Assert.assertTrue(messageQueue.peek().getId().equals("MSG-4"));

        System.out.println("\n--------------------------------------------------");
    }

    @Test
    public void testListenerNotifiedOnNothingToConsume() throws InterruptedException {
        System.out.println("\n***** TestMessageQueue.testListenerNotifiedOnNothingToConsume\n");

        CountDownLatch countDownLatch = new CountDownLatch(1);

        MessageQueue messageQueue = new MessageQueue();

        MessageQueueConsumer.Listener messageQueueConsumerListener = new LoggingMessageQueueConsumerListener();

        MessageQueueConsumer messageQueueConsumer = new MessageQueueConsumer(messageQueue);
        messageQueueConsumer.setListener(messageQueueConsumerListener);

        messageQueue.offer(new MockMessage("MSG-1", new TimeToLiveDefinite(2000)));
        messageQueue.offer(new MockMessage("MSG-2", new TimeToLiveDefinite(4000)));
        messageQueue.offer(new MockMessage("MSG-3", new TimeToLiveDefinite(2000)));

        messageQueueConsumer.start();

        countDownLatch.await(10000, TimeUnit.MILLISECONDS);

        messageQueueConsumer.stop();

        Assert.assertTrue(((LoggingMessageQueueConsumerListener) messageQueueConsumerListener).isOnNothingToConsumeCalled());

        System.out.println("\n--------------------------------------------------");
    }

    @Test
    public void testIndefiniteTimeToLiveLatestConsumedMessage() throws InterruptedException {
        System.out.println("\n***** TestMessageQueue.testIndefiniteTimeToLiveLatestConsumedMessage\n");

        CountDownLatch countDownLatch = new CountDownLatch(1);

        MessageQueue messageQueue = new MessageQueue();

        MessageQueueConsumer messageQueueConsumer = new MessageQueueConsumer(messageQueue);
        messageQueueConsumer.setListener(new LoggingMessageQueueConsumerListener());

        messageQueue.offer(new MockMessage("MSG-1", new TimeToLiveDefinite(2000)));
        messageQueue.offer(new MockMessage("MSG-2", new TimeToLiveDefinite(4000)));
        messageQueue.offer(new MockMessage("MSG-3", new TimeToLiveIndefinite()));

        messageQueueConsumer.start();

        countDownLatch.await(8000, TimeUnit.MILLISECONDS);
        messageQueue.offer(new MockMessage("MSG-4", new TimeToLiveDefinite(2000)));

        Assert.assertTrue(messageQueueConsumer.getLatestConsumedMessage().getId().equals("MSG-3"));
        Assert.assertTrue(messageQueue.peek().getId().equals("MSG-4"));

        messageQueueConsumer.stop();

        System.out.println("\n--------------------------------------------------");
    }

    @Test
    public void testRestartMessageQueueConsumer() throws InterruptedException {
        System.out.println("\n***** TestMessageQueue.testRestartMessageQueueConsumer\n");

        CountDownLatch countDownLatch = new CountDownLatch(1);

        MessageQueue messageQueue = new MessageQueue();

        MessageQueueConsumer messageQueueConsumer = new MessageQueueConsumer(messageQueue);
        messageQueueConsumer.setListener(new LoggingMessageQueueConsumerListener());

        messageQueue.offer(new MockMessage("MSG-1", new TimeToLiveDefinite(2000)));
        messageQueue.offer(new MockMessage("MSG-2", new TimeToLiveDefinite(4000)));
        messageQueue.offer(new MockMessage("MSG-3", new TimeToLiveIndefinite()));

        messageQueueConsumer.start();

        countDownLatch.await(8000, TimeUnit.MILLISECONDS);

        messageQueueConsumer.stop();
        messageQueue.clear();

        messageQueue.offer(new MockMessage("MSG-4", new TimeToLiveDefinite(2000)));

        messageQueueConsumer.start();

        Assert.assertTrue(messageQueueConsumer.getLatestConsumedMessage().getId().equals("MSG-4"));

        messageQueueConsumer.stop();

        System.out.println("\n--------------------------------------------------");
    }

}
