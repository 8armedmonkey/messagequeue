package com.eightam.mq;

import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class MessageQueueConsumer {

    private static final int THREAD_POOL_SIZE = 1;

    private MessageQueue mq;
    private MessageQueue.Listener mqListener;
    private Message latestConsumedMessage;
    private ScheduledExecutorService executorService;
    private boolean started;
    private Listener listener;

    private final Object lock;

    public MessageQueueConsumer(MessageQueue mq) {
        this.mq = mq;
        this.mqListener = new MessageQueueListener();
        this.lock = new Object();

        createExecutorService();
    }

    public void start() {
        synchronized (lock) {
            if (!started) {
                started = true;
                latestConsumedMessage = null;

                notifyListenerOnStart();
                registerMessageQueueListener();
                consume();
            }
        }
    }

    public void stop() {
        synchronized (lock) {
            if (started) {
                started = false;
                latestConsumedMessage = null;

                unregisterMessageQueueListener();
                shutdownAndRecreateExecutorService();
                notifyListenerOnStop();
            }
        }
    }

    public boolean isStarted() {
        return started;
    }

    public void consume() {
        synchronized (lock) {
            Message message = mq.poll();

            if (message != null) {
                TimeToLive timeToLive = message.getTimeToLive();

                if (timeToLive.getType() == TimeToLive.Type.DEFINITE) {
                    long delayMillis = ((TimeToLiveDefinite) timeToLive).getDurationMillis();
                    executorService.schedule(new ConsumeMessage(), delayMillis, TimeUnit.MILLISECONDS);
                }

                latestConsumedMessage = message;
                notifyListenerOnConsume(message);

            } else {
                latestConsumedMessage = null;
                notifyListenerOnNothingToConsume();

            }
        }
    }

    public Message getLatestConsumedMessage() {
        return latestConsumedMessage;
    }

    public void setListener(Listener listener) {
        this.listener = listener;
    }

    private void createExecutorService() {
        executorService = new ScheduledThreadPoolExecutor(THREAD_POOL_SIZE);
    }

    private void shutdownAndRecreateExecutorService() {
        executorService.shutdownNow();
        createExecutorService();
    }

    private boolean hasScheduledTasks() {
        return ((ScheduledThreadPoolExecutor) executorService).getQueue().size() > 0;
    }

    private boolean hasIndefiniteTimeToLiveLatestConsumedMessage() {
        return latestConsumedMessage != null && latestConsumedMessage.getTimeToLive().getType() == TimeToLive.Type.INDEFINITE;
    }

    private void registerMessageQueueListener() {
        mq.registerListener(mqListener);
    }

    private void unregisterMessageQueueListener() {
        mq.unregisterListener(mqListener);
    }

    private void notifyListenerOnStart() {
        if (listener != null) {
            listener.onStart();
        }
    }

    private void notifyListenerOnConsume(Message message) {
        if (listener != null) {
            listener.onConsume(message);
        }
    }

    private void notifyListenerOnNothingToConsume() {
        if (listener != null) {
            listener.onNothingToConsume();
        }
    }

    private void notifyListenerOnStop() {
        if (listener != null) {
            listener.onStop();
        }
    }

    public interface Listener {

        void onStart();

        void onConsume(Message message);

        void onNothingToConsume();

        void onStop();

    }

    class MessageQueueListener implements MessageQueue.Listener {

        @Override
        public void onMessageOffered() {
            if (started && !hasScheduledTasks() && !hasIndefiniteTimeToLiveLatestConsumedMessage()) {
                consume();
            }
        }

    }

    class ConsumeMessage implements Runnable {

        @Override
        public void run() {
            consume();
        }

    }

}
