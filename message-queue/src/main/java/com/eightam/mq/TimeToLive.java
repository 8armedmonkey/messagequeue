package com.eightam.mq;

public interface TimeToLive {

    Type getType();

    enum Type {

        DEFINITE, INDEFINITE

    }

}
